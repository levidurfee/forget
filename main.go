package main

import (
	"fmt"
	"github.com/golevi/forget/internal/encryption"
	"github.com/golevi/forget/internal/key"
)

// scrypt settings

func main() {
	password := "password"
	encryptedText, salt, nonce, err := encryption.Encrypt(password, "Hello World.. it is I, Levi")
	if err != nil {
		panic(err.Error())
	}

	fmt.Println("ENCRYPTED")
	fmt.Println(encryptedText, nonce, salt)
	fmt.Println()
	fmt.Println("DECRYPTED")

	plaintext, err := encryption.Decrypt(password, salt, nonce, encryptedText)
	if err != nil {
		panic(err.Error())
	}

	fmt.Println(plaintext)

	fmt.Println()

	randomPasswordSalt, err := key.Random()
	if err != nil {
		panic(err.Error())
	}

	// New naming convention/types might help describe the data. Root password,
	// save root salt, and nonce.
	//
	// Then call the other one payload data?
	randomPasswordSaltEncrypted, _, _, _ := encryption.Encrypt(password, randomPasswordSalt)

	fmt.Println(randomPasswordSalt, randomPasswordSaltEncrypted)
}
