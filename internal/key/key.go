package key

import (
	"crypto/rand"
	"github.com/golevi/forget/internal/encoding"
	"golang.org/x/crypto/scrypt"
)

const (
	N      = 1 << 15
	r      = 8
	p      = 1
	keyLen = 32
)

func Create(password, salt string) ([]byte, error) {
	byteSalt, err := encoding.Decode(salt)
	if err != nil {
		return []byte{}, nil
	}

	return scrypt.Key([]byte(password), byteSalt, N, r, p, keyLen)
}

func Random() (string, error) {
	byteSalt, err := randomBytes(18)
	if err != nil {
		return "", err
	}

	salt := encoding.Encode(byteSalt)

	bytePassword, err := randomBytes(18)
	if err != nil {
		return "", err
	}

	password := encoding.Encode(bytePassword)

	return password + "^" + salt, nil
}

func randomBytes(x int) ([]byte, error) {
	b := make([]byte, x)

	_, err := rand.Read(b)
	if err != nil {
		return []byte{}, err
	}

	return b, nil
}
